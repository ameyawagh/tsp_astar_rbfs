#!/usr/bin/python

from random import *

from solver import *

class TSP():
    def __init__(self, prevState=None):
        self.table = [(0,7,8,9),
                    (7,0,3,4),
                    (8,3,0,6),
                    (9,4,6,0)]
        
        self.f=0
        self.cities = range(4)
        shuffle(self.cities)
        self.state = self.cities+[self.cities[0]]
        self.goalState = (2,1,3,0,2)
        self.parent=None
        if prevState is not None:
            self.state = tuple(prevState)
        else:
            print "Initial state"
            self.state = tuple(self.state)

    def isGoalReached(self):
        return (self.state == self.goalState)

    def getMSTDist(self,nieghbourState=None):
        if nieghbourState is not None:
            _newState = nieghbourState[:]
        else:
            _newState = self.state[:]

        MSTdist=0
        for i in range(len(_newState)-1):
            MSTdist += self.table[_newState[i]][_newState[i+1]]
            # print table[state[i]][state[i+1]]
        # print 'MST',MSTdist
        return MSTdist    

    def swapCities(self,state, index1,index2):
        newState = state[:]
        tempCity = newState[index1]
        newState[index1] = newState[index2]
        newState[index2] = tempCity
        print newState
        return newState


    def getNeighbour(self):
        # neighbourList = []
        # self.parent = self
        self.temp = list(self.state[:-1])
        print len(self.temp)
        # shuffle(self.temp)
        indexCenter = randint(0,len(self.temp)-1)
        indexLeft = (indexCenter-1)%(len(self.temp))
        indexRight = (indexCenter+1)%(len(self.temp))
        
        print indexLeft,indexCenter,indexRight

        neighbourLeft = self.swapCities(self.temp,indexLeft,indexCenter)
        neighbourRight = self.swapCities(self.temp,indexRight,indexCenter)

        neighbourLeft += [neighbourLeft[0]]
        neighbourRight += [neighbourRight[0]]
        # self.explored.add(tuple(self.temp))
        neighbourLeft = TSP(neighbourLeft)
        neighbourRight = TSP(neighbourRight)

        neighbourLeft.parent = self        
        neighbourRight.parent = self

        return [neighbourLeft,neighbourRight]

    def printNode(self):
        print self.state

    def __hash__(self):
        return hash(self.state)

if __name__ == '__main__':
    try:
        tsp=TSP()
        print tsp.state
        print tsp.goalState
        print tsp.getMSTDist()
        print tsp.isGoalReached()
        leafNode , exploredAstar = Astar(TSP,tsp.state)
        print leafNode.state,'MST:',leafNode.getMSTDist()
        print '-'*80
        print "\t\t Output of A*"
        print '-'*80
        printBranch(leafNode)
        print 'explored nodes in A*:',len(exploredAstar)

        raw_input("press any key to continue")

        leafNode , exploredRBFS = Astar(TSP,tsp.state)
        print leafNode.state,'MST:',leafNode.getMSTDist()
        print '-'*80
        print "\t\t Output of RBFS"
        print '-'*80
        printBranch(leafNode)
        print 'explored nodes in RBFS:',len(exploredRBFS)


    except KeyboardInterrupt:
        quit()